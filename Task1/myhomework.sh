# Create Network:
docker network create --attachable network-san
# Postgres Configuration:
docker volume create --name postgres_data
docker container create --name postgres-san -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=Control123 -e PGDATA=/var/lib/postgresql/data/pgdata -v postgres_data:/var/lib/postgresql/data --network network-san postgres:latest
docker start postgres-san
# Sonarqube Configuration:
sysctl -w vm.max_map_count=524288
sysctl -w fs.file-max=131072
ulimit -n 131072
ulimit -u 8192
docker volume create --name sonarqube_data
docker volume create --name sonarqube_logs
docker volume create --name sonarqube_extensions
docker container create --name sonarqube-san -p 9000:9000 -e SONAR_JDBC_URL=jdbc:postgresql://postgres-san/sonar -e SONAR_JDBC_USERNAME=postgres -e SONAR_JDBC_PASSWORD=Control123 -v sonarqube_data:/opt/sonarqube/data -v sonarqube_extensions:/opt/sonarqube/extensions -v sonarqube_logs:/opt/sonarqube/logs --network network-san sonarqube:latest
docker start sonarqube-san
# I could no access to Web UI in Sonarqube
# Jenkins Configuration:
docker pull jenkins/jenkins:lts-jdk11
docker run -d --name jenkins-san --network network-san -v jenkins_home:/var/jenkins_home -p 8080:8080 -p 50000:50000 jenkins/jenkins:lts-jdk11
# Sonatype Configuration:
docker pull sonatype/nexus3
docker volume create --name nexus-data
docker run -d --name sonatype-san --network network-san -p 8081:8081 -v nexus-data:/nexus-data sonatype/nexus3 
# Portainer Configuration:
docker pull portainer/portainer
docker volume create portainer_data
docker run -d -p 8000:8000 -p 9001:9001 --name=portainer-san --network network-san --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
# #Interactive for postgres:
# docker exec -it  postgres-san bash
# psql -h postgres-san -U postgres
# #Password: Control123
# Control123
# #Databases List
# \l
# exit # Exit PSQL
# exit # Exit Bash
docker ps -a
docker network inspect network-san