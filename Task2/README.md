# Docker Training | Homework 2
> _Docker - 08/01/2021_

Project node JS (Task2 Folder): https://gitlab.com/sandy.garcia.jala/jala-docker/-/tree/master
Go to project folder and run the following commands
- `docker build .`
- `docker build -t node-web-app:sandy.garcia .`
- `docker run -d --name node-web-app -p 9090:8080 node-web-app:sandy.garcia`
- `docker ps`
- `docker stop node-web-app`
##### docker.hub: https://hub.docker.com/repository/docker/ragyd/node-web-app
- `docker login`
- `docker tag node-web-app:sandy.garcia ragyd/node-web-app:sandy.garcia`
- `docker push ragyd/node-web-app:sandy.garcia`
##### docker.jala.pro: https://docker.jala.pro/harbor/projects/73/repositories/node-web-app
- `docker tag node-web-app:sandy.garcia docker.jala.pro/docker-training/node-web-app:jalasoft`
- `docker login docker.jala.pro`
- `docker push docker.jala.pro/docker-training/node-web-app:jalasoft`
##### 10.24.48.191:8081 Sonatype Nexus: http://10.24.48.191:8081/#browse/search/docker:294c6332b49a6d45f204e9a61d83db17
Active  10.24.48.191
- `ls /etc/docker`
- `sudo vim /etc/docker/daemon.json`
{
"insecure-registries":["10.24.48.191:8082"]
}
- `sudo systemctl stop docker`
- `sudo systemctl start docker`
- `docker login 10.24.48.191:8082`
- `docker tag node-web-app:sandy.garcia 10.28.48.191:8082/node-web-app:sandy.garcia`
- `docker push 10.24.48.191:8082/node-web-app:sandy.garcia`
- `docker images`
